# Changelog for polysemy-kvstore

## v0.1.2.0

* Add `runKVStoreAsKVStore` and `runKVStoreAsKVStoreSem`.

## v0.1.0.0

* Imported `KVStore` effect from polysemy-zoo.
